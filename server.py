from subprocess import Popen, PIPE
import logging
import os
#from time import sleep # for test
from flask import Flask, Response, Request, render_template, redirect, url_for, make_response, request
from utils.utils import *

MANAGER_PATH = '/home/admin/wifi'
EF_DIRS_PATH = '/home/admin/opi-ef-projects'
CONFIG_FILENAME = 'project.txt'
OUTPUT_CONFIG_FILENAME = 'out-params.txt'
CONNECTIONS_DIR = '/etc/NetworkManager/system-connections'
LOGFILE = '/home/admin/wifi/_logs/manager.log'

app = Flask(__name__)

if not app.debug:
  logging.basicConfig(filename=LOGFILE,
       level=logging.DEBUG, format='%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')

@app.get('/')
def ping_device():
    return 'pong'

@app.get('/ef-central')
def root_handler():
    dirs = list_directories(EF_DIRS_PATH, app.logger)
    return render_template(
        'root.html', projects=dirs
    )

@app.get('/default/<default_project>')
def default_handler(default_project):
    dirs = list_directories(EF_DIRS_PATH, app.logger)
    if default_project in dirs:
        write_default_project(EF_DIRS_PATH, CONFIG_FILENAME, default_project, app.logger)
        ps = Popen('sync', shell=True)
        ps.wait()
        return render_template(
            "default.html", default_project=default_project
        )
    app.logger.debug('No such project {}, redirecting...'.format(default_project))
    return redirect(url_for('/ef-central'))

@app.get('/stop-play')
def stop_play():
    if kill_current_player():
        return 'Stopped'
    else:
        return 'Is not playing'

@app.get('/start-play')
def start_play():
    if check_is_playing():
      return 'Already playing'
    ps = Popen('cd {0} && sudo {1} && cd {2}'.format(
       EF_DIRS_PATH, os.path.join(EF_DIRS_PATH, 'start-play.sh'), MANAGER_PATH),
       shell=True, stdout=PIPE)
    return 'Started'

@app.get('/output/<default_output>')
def default_output_handler(default_output):
    if default_output in ['0','1']:
        write_default_output(EF_DIRS_PATH, OUTPUT_CONFIG_FILENAME, default_output, app.logger)
        ps = Popen('sync', shell=True)
        ps.wait()
        return 'Default output set: {}'.format(default_output)
    app.logger.debug('No such device output {}, redirecting...'.format(default_output))
    return redirect(url_for('/ef-central'))


'''
***********************************
Normal API
***********************************
'''

@app.get('/api/ef-central')
def api_list_projects():
    '''
    Outputs available EF projects
    '''
    dirs = list_directories(EF_DIRS_PATH, app.logger)
    return [{d: True} for d in dirs]

@app.post('/api/default/<default_project>')
def api_select_project(default_project):
    '''
    Select project to play
    '''
    dirs = list_directories(EF_DIRS_PATH, app.logger)
    if default_project in dirs:
        write_default_project(EF_DIRS_PATH, CONFIG_FILENAME, default_project, app.logger)
        ps = Popen('sync', shell=True)
        ps.wait()
        return 'OK'
    return make_response('No such project {}, error!'.format(default_project), 400)

@app.post('/api/output/<default_output>')
def api_default_output(default_output):
    '''
    Select device output (analog jack or SPDIF)
    '''
    if default_output in ['0','1']:
        write_default_output(EF_DIRS_PATH, OUTPUT_CONFIG_FILENAME, default_output, app.logger)
        ps = Popen('sync', shell=True)
        ps.wait()
        return 'Default output set: {}'.format(default_output)
    return make_response('No such device output {}, error!'.format(default_output), 400)

@app.post('/api/stop-play')
def api_stop_play():
    '''
    Stop playing
    '''
    if kill_current_player():
        return 'Stopped'
    else:
        return make_response('Is not playing', 400)

@app.post('/api/start-play')
def api_start_play():
    '''
    Start playing current project
    '''
    if check_is_playing():
      return make_response('Already playing', 400)
    ps = Popen('cd {0} && sudo {1} && cd {2}'.format(
       EF_DIRS_PATH, os.path.join(EF_DIRS_PATH, 'start-play.sh'), MANAGER_PATH),
       shell=True, stdout=PIPE)
    return 'Started'

@app.get('/api/reboot')
def reboot_handler():
    '''
    Reboot device
    '''
    ps = Popen('sudo reboot', shell=True, stdout=PIPE)
    return 'Rebooting...'

@app.get('/api/shutdown')
def shutdown_handler():
    '''
    Turn off device
    '''
    ps = Popen('sudo halt', shell=True, stdout=PIPE)
    return 'Shutdown...'

@app.get('/api/check_update')
def check_for_update_projects():
    '''
    Check if projects have new updates
    '''
    def streamer(ps):
        for line in iter(ps.stdout.readline, b''):
            yield line.decode()

    ps = Popen('su -c "/home/admin/wifi/scripts/check_update.sh" - admin', shell=True, stdout=PIPE)
    return Response(streamer(ps))

@app.get('/api/update')
def update_projects():
    '''
    Update projects from git repo
    '''
    def streamer(ps):
        for line in iter(ps.stdout.readline, b''):
            yield line.decode()
            #sleep(1) # for test
            #yield "<p>{}</p>".format(line.decode()) # for browser

    kill_current_player()
    ps = Popen('su -c "/home/admin/wifi/scripts/update.sh" - admin', shell=True, stdout=PIPE)
    return Response(streamer(ps))

@app.get('/api/name')
def get_device_name():
    '''
    Get device name
    '''
    ps = Popen('cat /etc/hostname', shell=True, stdout=PIPE)
    name = ps.stdout.read().decode().strip()
    return name

@app.get('/api/connections')
def get_connections_list():
    '''
    Get wifi connections list
    '''
    conns = list_connections(CONNECTIONS_DIR, app.logger)
    return [{c: True} for c in conns]

@app.delete('/api/connection/<conn_name>')
def delete_connection(conn_name):
    '''
    Delete wifi connection
    '''
    conns = list_connections(CONNECTIONS_DIR, app.logger)
    full_conn_name = conn_name + '.nmconnection'
    if full_conn_name in conns:
        if len(conns) == 1:
            return make_response('This is your last connection!', 400)

        app.logger.debug('Deleting connection {}...'.format(conn_name))
        # delete here
        command = 'nmcli connection delete {}'.format(conn_name)
        ps = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)
        err = ps.stderr.read()
        if err:
            return make_response('Error: {}'.format(err.decode()), 400)
        ps.wait()
        return 'OK'

    return make_response('No such connection {}!'.format(conn_name), 400)

@app.post('/api/create-connection')
def create_new_connection():
    '''
    Create new wifi connection
    '''
    data = request.json
    if not data:
        return make_response('No data!', 400)
    conn_name = data.get('name')
    passwd = data.get('password')
    if not conn_name or not passwd:
        return make_response('Bad data!', 400)
    if len(conn_name) < 10:
        return make_response('Bad name!', 400)
    if len(passwd) < 8:
        return make_response('Bad password!', 400)
    full_conn_name = conn_name + '.nmconnection'
    conns = list_connections(CONNECTIONS_DIR, app.logger)
    if full_conn_name in conns:
        app.logger.debug('Connection {} exists!'.format(conn_name))
        return make_response('Connection {} exists!'.format(conn_name), 400)
    app.logger.debug('Creating connection {}...'.format(conn_name))
    # create
    command = 'nmcli con add con-name {0} ifname wlan0 type wifi ssid {1}'.format(
	    conn_name, conn_name)
    ps = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)
    err = ps.stderr.read()
    if err:
        return make_response('Error: {}'.format(err.decode()), 400)
    ps.wait()
    # setup1
    command = 'nmcli con modify {} wifi-sec.key-mgmt wpa-psk'.format(conn_name)
    ps = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)
    err = ps.stderr.read()
    if err:
        return make_response('Error: {}'.format(err.decode()), 400)
    ps.wait()
    # setup2
    command = 'nmcli con modify {0} wifi-sec.psk {1}'.format(
       conn_name, passwd)
    ps = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)
    err = ps.stderr.read()
    if err:
        return make_response('Error: {}'.format(err.decode()), 400)
    ps.wait()

    return 'OK'

@app.get('/api/check_update_server')
def check_for_update_server():
    '''
    Check if projects have new updates
    '''
    def streamer(ps):
        for line in iter(ps.stdout.readline, b''):
            yield line.decode()

    ps = Popen('su -c "/home/admin/wifi/scripts/check_update_server.sh" - admin', shell=True, stdout=PIPE)
    return Response(streamer(ps))

@app.get('/api/update_server')
def update_server():
    '''
    Update projects from git repo
    '''
    def streamer(ps):
        for line in iter(ps.stdout.readline, b''):
            yield line.decode()
            #sleep(1) # for test
            #yield "<p>{}</p>".format(line.decode()) # for browser

    ps = Popen('su -c "/home/admin/wifi/scripts/update_server.sh" - admin', shell=True, stdout=PIPE)
    return Response(streamer(ps))
