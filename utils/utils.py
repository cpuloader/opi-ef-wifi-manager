import os
from subprocess import Popen, PIPE

def list_directories(path, logger):
    try:
        dirs = [d for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))]
        filtered_dirs = [d for d in dirs if d.endswith('gen')]
        #print("Dirs: {}".format(filtered_dirs))
        logger.debug("Dirs: {}".format(filtered_dirs))
        return filtered_dirs
    except Exception as e:
        print("An error occurred: ", str(e))
        return []

def write_default_project(path, filename, default_name, logger):
    try:
        fullpath = os.path.join(path,filename)
        with open(fullpath, 'w') as file:
            file.write(default_name)
            logger.debug("Successfully written to {}".format(fullpath))
        return True
    except Exception as e:
        print("An error occurred: ", str(e))
        return False

def kill_current_player():
    ps = Popen('ps -e | grep play-release', shell=True, stdout=PIPE)
    out = ps.stdout.read().decode()
    index = out.find('?')
    if index != -1:
        pid = out[:index].strip()
        ps = Popen('sudo kill -9 {}'.format(pid), shell=True, stdout=PIPE)
        return True
    return False

def check_is_playing():
    ps = Popen('ps -e | grep play-release', shell=True, stdout=PIPE)
    out = ps.stdout.read().decode()
    index = out.find('?')
    if index != -1:
        return True
    else:
        return False

def write_default_output(path, filename, default_output, logger):
    try:
        fullpath = os.path.join(path,filename)
        with open(fullpath, 'w') as file:
            file.write(default_output)
            logger.debug("Successfully written output device to {}".format(fullpath))
        return True
    except Exception as e:
        print("An error occurred: ", str(e))
        return False

def list_connections(path, logger):
    try:
        files = [d for d in os.listdir(path)]
        #print("Path: {0} Files: {1}".format(path, files))
        filtered_files = [d for d in files if d.endswith('nmconnection')]
        logger.debug("Connections: {}".format(filtered_files))
        return filtered_files
    except Exception as e:
        print("An error occurred: ", str(e))
        return []
