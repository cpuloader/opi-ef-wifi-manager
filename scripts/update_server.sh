#!/usr/bin/env bash

cd /home/admin/wifi
set -x #echo on

git fetch --all
git reset --hard origin/main

# Test:
#git status
set +x

cd /home/admin/wifi
